# Starwars-games

### Premier projet formation AJC

### Description : 

Cette application lance un jeu sous forme de grille sur la console. Vous controlez un personnage et le but est de tuer tous les ennemis pr�sent sur la grille !

J'ai fait ce jeu en dotnet car j'apprends ce langage.


** IMPORTANT ! **
Ceci est la version 0.1, le jeu n'est pas fini.



### Comment installer le jeu : 

D�zippez le et lancez le jeu en double cliquant sur le fichier "starwars-game.sln" !



### Liens Gitlab : 

Lien du projet : https://gitlab.com/waritpark/formation-ajc-dotnet-20032023Lien 
Lien du formateur : https://gitlab.com/devtobecurious


<!-- c'est plus joli sur github : https://github.com/waritpark/Starwars-games/blob/master/README.md -->