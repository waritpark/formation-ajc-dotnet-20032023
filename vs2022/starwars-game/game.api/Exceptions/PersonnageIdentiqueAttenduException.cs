﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace game.api.Exceptions
{
    /// <summary>
    /// A déclencher quand deux persos de jeu sont identiques
    /// </summary>
    public class PersonnageIdentiqueAttenduException : Exception
    {

    }
}
