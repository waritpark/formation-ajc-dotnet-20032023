﻿namespace decouverte_delegates
{
    /// <summary>
    /// Contrat de méthode pour afficher un item
    /// </summary>
    /// <param name="item"></param>
    delegate void Afficher(object item); // le quoi : la signature de votre methode, c'est un nouveau type

    /// <summary>
    /// Contrat de méthode pour lire la saisie
    /// </summary>
    /// <returns></returns>
    delegate string LectureSaisie();
}