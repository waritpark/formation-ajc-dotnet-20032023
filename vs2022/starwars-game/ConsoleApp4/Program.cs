﻿using decouverte_delegates;

void CalculerSomme (Afficher afficher, params int[] values)
{
    int result = values.Sum();

    afficher(result);//je ne sais pas ici comment sa va etre affiché, pas le role/responsabilité
}

void AfficheEnCouleur(object item, ConsoleColor couleur)
{
    Console.WriteLine(item);
    Console.ForegroundColor = ConsoleColor.White;
}
void AfficherEnRouge(object item)
{
    Console.ForegroundColor = ConsoleColor.Red;
}

void AfficherEnVert(object item)
{
    Console.ForegroundColor = ConsoleColor.Green;
}

CalculerSomme(Console.WriteLine, 1, 2, 3, 4);
CalculerSomme(AfficherEnVert, 2, 3, 4);

#region Etude de cas d'un delegue
Afficher afficher = Console.WriteLine;
Afficher autreChose = afficher;

LectureSaisie read = Console.ReadLine;

afficher("coucou");
#endregion
