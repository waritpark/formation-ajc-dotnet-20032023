﻿using game.api;
using Microsoft.AspNetCore.Mvc;
using starwarsgame.Web.UI.Infrastructures;
using starwarsgame.Web.UI.Models;
using System.Reflection;
using WebApplication1.Models;

namespace starwarsgame.Web.UI.Controllers
{
	public class GameController : Controller
	{
		private readonly DefaultDbContext context;
		// Parceque j'ai ajouté la ligne AddDbContext dans le routeur
		// ici on va récupérer une instance deja prete
		public GameController(DefaultDbContext context)
		{
			this.context = context;
		}
		public IActionResult Index()
		{
			var sauvegardeur = new BddGameSauvegarde();

			List<Game> games = new()
			{
				new(sauvegardeur) { Id = 1, CheckPointList = new() { new(1, 150) } },
				new(sauvegardeur) { Id = 2, CheckPointList = new() { new(2, 200) } }
			};

			return View(new Models.GameListViewModel()
			{
				GameList = games
			});
		}

		[HttpGet]
		public IActionResult Add()
		{
			return View();
		}

		[HttpPost]
		public IActionResult Add(GameAddViewModel item)
		{
			//string value = this.Request.Form["Prenom"];
			this.context.Games.Add(item.Game);
			this.context.SaveChanges(); // on limite l'acces à la base

			return RedirectToAction("Index");
		}
	}
}
