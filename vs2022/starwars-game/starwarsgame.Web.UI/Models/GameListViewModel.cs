﻿using game.api;

namespace starwarsgame.Web.UI.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class GameListViewModel
    {
        public List<Game> GameList { get; set; } = new();
    }
}
