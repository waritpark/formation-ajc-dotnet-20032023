﻿using game.api;

namespace starwarsgame.Web.UI.Models
{
	public class GameAddViewModel
	{
        public Game Game { get; set; }
    }
}
