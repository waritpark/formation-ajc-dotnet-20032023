﻿using tva.api.Calculateurs;
using tva.api.Displayers;

Console.OutputEncoding = System.Text.Encoding.UTF8; // gérer le signe €

// TvaCalculateur calculateur = new TvaCalculateur(); = TvaCalculateur calculateur = new ();

TvaCalculateur calculateur = new ();
TvaDisplayers displayer = new ();

List<decimal> tvaList = new();

void AfficherEnVert(object message)
{
    Console.ForegroundColor = ConsoleColor.Green;
    Console.WriteLine (message);
    Console.ForegroundColor = ConsoleColor.White;
}
// une fonction anonyme = expression lambda avec cette syntaxe seulement quand il y a 1 param
calculateur.InitialiserListe(tvaList, Console.ReadLine, mess => Console.WriteLine(mess.ToString()));

displayer.Display(tvaList, Console.WriteLine);

var tvasFiltrees = calculateur.FiltrerTVA(tvaList);

displayer.Display(tvasFiltrees, Console.WriteLine);

var montantTTC = calculateur.CalculerTTC(20, () => 0.2M);