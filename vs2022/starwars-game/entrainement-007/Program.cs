﻿using entrainement_007;

Personne personne = new();
Ambulance ambulance = new();
Pompier pompier = new();

personne.detecteFeu += pompier.PompiersArrive;
personne.detecteFeu += ambulance.AmbulancePart;
personne.detecteFeu += ambulance.AmbulanceArrive;

personne.Appel();