﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace entrainement_007
{
    internal delegate void DetecteFeu(Personne personne, Appel appelPompier);
    internal class Personne
    {
        public event DetecteFeu detecteFeu;

        public void Appel()
        {
            Console.WriteLine("Il y a le feu dans ma maison !");
            Appel appel1 = new() { Name = "Help me Pompiers" };
            this.detecteFeu?.Invoke(this, appel1);
            Console.WriteLine("Help me ami !");

        }

    }
}
