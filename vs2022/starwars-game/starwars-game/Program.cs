﻿
using game.api;
using game.infrastructure;
using grille.api;



// Titre
Console.ForegroundColor = ConsoleColor.DarkGreen;
var monTitre = "a starwars game".ToUpper();
Console.WriteLine(monTitre);
Console.ForegroundColor = ConsoleColor.White;

// gérer le placement aléatoire des ennemies
var rand = new Random();
int r1 = rand.Next(6);
int r2 = rand.Next(6);
int r3 = rand.Next(6);
int r4 = rand.Next(6);
int r5 = rand.Next(6);
int r6 = rand.Next(6);

NouvellePartie();

void NouvellePartie()
{
	// Creation du jeu
	var tablejeu = new Grille(8, 7);

	// Joueur
	BasePersonnage persoPlayer = new PersoPrincipal(Console.WriteLine, Console.ReadLine);
	Position2D positionPlayer = new (0,3);
	tablejeu.Placement(positionPlayer, "O");

	// Sauvegarde
	string cheminSauvegarde = Path.Combine(Environment.CurrentDirectory, $"checkpoint-save-{DateTime.Now.ToString("yyyy-MM-dd-hh-mm-ss")}.json");
	var game = new Game(new JsonGameSaver(cheminSauvegarde));
	game.Demarrer(persoPlayer);
	try
	{
		game.Sauvegarder();
	}
	catch (NotImplementedException ex)
	{
		Console.ForegroundColor = ConsoleColor.Green;
		Console.WriteLine("Oops erreur !", ex.Message);
		Console.ForegroundColor = ConsoleColor.White;
	}
	catch (Exception ex)
	{

	}

	// ennemies
	Position2D positionEnnemi1 = new(r1, r2);
	Position2D positionEnnemi2 = new(r3, r4);
	Position2D positionEnnemi3 = new(r5, r6);
	do
	{
		int r1 = rand.Next(8);
		int r2 = rand.Next(7);
		int r3 = rand.Next(8);
		int r4 = rand.Next(7);
		int r5 = rand.Next(8);
		int r6 = rand.Next(7);
	} while (positionEnnemi1 == positionPlayer || positionEnnemi2 == positionPlayer || positionEnnemi3 == positionPlayer || positionEnnemi1 == positionEnnemi2 || positionEnnemi1 == positionEnnemi3 || positionEnnemi2 == positionEnnemi3);
	BasePersonnage ennemi1 = new Ennemi();
	BasePersonnage ennemi2 = new Ennemi();
	BasePersonnage ennemi3 = new Ennemi();
	tablejeu.Placement(positionEnnemi1, "X");
	tablejeu.Placement(positionEnnemi2, "X");
	tablejeu.Placement(positionEnnemi3, "X");

	// Affichage du jeu
	Console.WriteLine("\n");
	Console.WriteLine(tablejeu.AfficherGrille());

	// Deplacement du joueur
	persoPlayer.SeDeplacer();

}





