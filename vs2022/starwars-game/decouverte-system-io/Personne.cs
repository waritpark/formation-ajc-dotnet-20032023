﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace decouverte_system_io
{
    public class Personne
    {
        public string Name { get; set; }
        public int Id { get; set; }
    }
}
