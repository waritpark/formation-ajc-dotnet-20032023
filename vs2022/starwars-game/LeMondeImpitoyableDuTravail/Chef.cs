﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeMondeImpitoyableDuTravail
{
    internal class Chef
    {
        public string Prenom { get; set; } = "";

        public void InspecterTravail(Salarie salarie, Code code)
        {
            Console.WriteLine("Travail très intéressant ! {0}", this.Prenom);
        }
    }
}
