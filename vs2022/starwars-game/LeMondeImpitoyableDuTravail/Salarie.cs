﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeMondeImpitoyableDuTravail
{
    internal delegate void TravailFini(Salarie salarie, Code code);

    internal class Salarie
    {
        // public event Action<Salarie, Code> travailFini;
        public event TravailFini travailFini;

        //public List<TravailFini> LesAbonnes = new List<TravailFini>();

        public void Ecrire(Salarie autreSalarie, Code codeExistant)
        {
            Console.WriteLine("J'écris du code");
            Code code = new() { Contenu = "<html></html>" };

            // this.travailFini(this, code);
            this.travailFini?.Invoke(this, code);// Si aucun abonné, on exécute pas l'événement

            // this.LesAbonnes.ForEach(methodAExecutee => methodAExecutee(this, code));
            //foreach (var methodeAExecutee in this.LesAbonnes)
            //{
            //    methodeAExecutee(this, code);
            //}

        }
    }
}
