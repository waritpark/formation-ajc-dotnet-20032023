﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jeu_entrainement.api
{
    public class Ennemi : Identity
    {
        #region Constructor
        public Ennemi(string name, int pv, int force, decimal coordonates) : base(name, pv, force, coordonates)
        {

        }
        #endregion

        #region Methods
        public override void Attaquer(Identity identity)
        {
            base.Attaquer(identity);
            Console.WriteLine($"{this.Name} vous attaque ! !");
        }
        public override void SeDefendre(Identity identity)
        {
            base.SeDefendre(identity);
            Console.WriteLine($"{this.Name} esquive votre attaque !");
        }
        public override string ToString()
        {
            return $"{this.Name}";
        }
        #endregion
    }
}
