﻿namespace starwarsgame.api
{
    public class Menu
    {
        #region Fields
        public string NouvellePartie = "1. Nouvelle partie";
        public string ChargerPartie = "2. Charger partie";
        public string Options = "3. Options";
        public string Quitter = "0. Quitter";
        private int readMenu;
        #endregion

        #region Properties
        public int ReadMenu { get => readMenu; set => readMenu = value; }
        #endregion

        #region Constructor
        public Menu()
        {
            //AfficherMenu();
            Console.WriteLine($"{NouvellePartie} / {ChargerPartie} / {Options} / {Quitter}");
            this.readMenu = int.Parse(Console.ReadLine());
        }
        #endregion

        #region Methods
        public void AfficherMenu()
        {
            //Console.WriteLine($"{NouvellePartie} / {ChargerPartie} / {Options} / {Quitter}");
            //this.readMenu = int.Parse(Console.ReadLine());

        }

        #endregion

    }
}