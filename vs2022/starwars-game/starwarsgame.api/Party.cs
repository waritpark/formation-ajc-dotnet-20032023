﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jeu_entrainement.api
{
    public class Party : Identity
    {
        #region Fields
        private DateTime dateStart;
        private string checkPoint; 
        #endregion

        #region Properties
        public DateTime DateStart { get => dateStart; set => dateStart = value; }
        public string CheckPoint { get => checkPoint; set => checkPoint = value; }
        #endregion

        #region Constructor
        public Party(int pv, decimal coordonates, DateTime dateStart) : base(pv, coordonates)
        {
            this.dateStart = DateTime.Now;
            this.checkPoint = $"{DateStart}.{PV}.{Coordonates}";
        }
        #endregion

        #region Methods
        public override string ToString()
        {
            return $"Checkpoint de la partie : {checkPoint}";
        }
        #endregion



    }
}
