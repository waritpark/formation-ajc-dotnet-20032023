﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace menu.api
{
	/// <summary>
	/// Menu avec ses item
	/// </summary>
	public class Menu
	{
		#region Fields
		private readonly Action<string> afficherInfo;
		private readonly Func<string> recupererInfo;
		private IAfficheur afficheur;
		#endregion

		#region Constructors
		public Menu(Action<string> afficher, Func<string> recupererInfo)
		{
			this.afficherInfo = afficher;
			this.recupererInfo = recupererInfo;
		}
		public Menu(IAfficheur afficheur)
		{
			this.afficheur = afficheur;
		}
		#endregion

		#region Public methods
		public void Afficher()
		{
			foreach (var item in this.Items.OrderBy(item => item.OrdreAffichage))
			{
				this.afficheur.Afficher(item.ToString());
			}
		}

		public void Ajouter(MenuItem item)
		{
			this.Items.Add(item);
		}
		#endregion

		#region Properties
		public List<MenuItem> Items { get; private set; } = new();
		#endregion
	}
}
