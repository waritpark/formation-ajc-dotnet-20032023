﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace menu.api
{
	public interface IAfficheur
	{
		void Afficher(string message);
	}
}
