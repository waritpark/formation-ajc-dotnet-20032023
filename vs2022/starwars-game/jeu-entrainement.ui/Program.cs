﻿using jeu_entrainement.api;

var player1 = new Player("Francois", 100, 25, 4545);
var partie1 = new Party(100,4545,DateTime.Now);
var ennemi1 = new Ennemi("Squelette", 25, 15, 5050);

Console.WriteLine(partie1);

player1.Attaquer(ennemi1);

ennemi1.SeDefendre(partie1); 