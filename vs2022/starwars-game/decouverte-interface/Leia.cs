﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace decouverte_interfaces
{
    internal class Leia : IQuiPeutVoler
    {
        public void Voler()
        {
            Console.WriteLine("Je vole alors que je suis morte !");
        }

    }
}
