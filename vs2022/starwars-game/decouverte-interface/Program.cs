﻿using decouverte_interfaces;

//OiseauQuiVole oiseau = new Perroquet();

//oiseau.Voler();

//Console.ReadLine();

//Oiseau oiseau2 = new Kiwi();

List<IQuiPeutVoler> list = new()
{
    new Perroquet(),
    new ChauveSouris(),
    new Leia()
};
list.ForEach(item => item.Voler());
