﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace decouverte_interfaces
{
    internal interface IQuiPeutVoler
    {
        void Voler();
    }
}
