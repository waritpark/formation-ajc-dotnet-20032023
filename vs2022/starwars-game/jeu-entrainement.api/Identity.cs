﻿namespace jeu_entrainement.api;

public abstract class Identity
{
    #region Fields
    private string name = "";
    private int pv;
    private int force;
    private decimal coordonates;
    #endregion

    #region Properties
    public string Name { get => name; set => name = value; }
    public int PV { get => pv; set => pv = value; }
    public int Force { get => force; set => force = value; }
    public decimal Coordonates { get => coordonates; set => coordonates = value; }
    #endregion

    #region Constructor
    public Identity(int pv, decimal coordonates)
    {
        this.PV = pv;
        this.Coordonates = coordonates;
    }
    public Identity(string name, int pv, int force, decimal coordonates)
    {
        this.Name = name;
        this.PV = pv;
        this.Force = force; 
        this.Coordonates = coordonates;
    }
    #endregion

    #region methods
    public virtual void Attaquer(Identity identity)
    {
        //Console.WriteLine("J'attaque !");
    }
    public virtual void SeDefendre(Identity identity)
    {
        //Console.WriteLine("Attaque esquivé !");
    }
    #endregion

}