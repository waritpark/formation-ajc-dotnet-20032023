﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jeu_entrainement.api
{
    public class Player : Identity
    {
        #region Constructor
        public Player(string name, int pv, int force, decimal coordonates) : base(name, pv, force, coordonates)
        {

        }
        #endregion

        #region Methods
        public override void Attaquer(Identity identity)
        {
            base.Attaquer(identity);
            Console.WriteLine("J'attaque à l'épée !");
        }
        public override void SeDefendre(Identity identity)
        {
            base.SeDefendre(identity);
            Console.WriteLine("J'esquive l'attaque !");
        }
        public override string ToString()
        {
            return $"{this.Name}";
        }
        #endregion
    }
}
