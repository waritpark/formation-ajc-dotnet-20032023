﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jeu_entrainement.api
{
    public class Party : Identity
    {
        #region Fields
        private DateTime dateStart;
        private string checkPoint; 
        #endregion

        #region Properties
        public DateTime DateStart { get => dateStart; set => dateStart = value; }
        public string CheckPoint { get => checkPoint; set => checkPoint = value; }
        #endregion

        #region Constructor
        public Party(int pv, decimal coordonates, DateTime dateStart) : base(pv, coordonates)
        {
            this.dateStart = dateStart;
            this.checkPoint = $"{DateStart}.{PV}.{Coordonates}";
            //this.checkPoint = checkPoint;
        }

        public override string ToString()
        {
            return $"Checkpoint de la partie : {checkPoint}";
        }

        public void DemarrerPartie()
        {
            this.dateStart = DateTime.Now;
        }

        public void ChargerPartie()
        {
            Console.WriteLine("fonctionnalité pas développé 'charger une partie' ");

        }

        public void OptionsPartie()
        {
            Console.WriteLine("fonctionnalité pas développé 'options' ");
        }
        #endregion


    }
}
