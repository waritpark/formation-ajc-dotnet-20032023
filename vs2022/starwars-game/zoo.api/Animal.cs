﻿namespace zoo.api;

public abstract class Animal
{
    #region Fields
    //private AnimalType type;
    private string name = "";
    private string zone = "";
    private int portee;
    private int pv;
    private int force;
    private bool hostile;
    #endregion

    #region Properties
    //public AnimalType Type { get => type; private set => type = value; }
    public string Name { get => name; set => name = value; }
    public string Zone { get => zone; set => zone = value; }
    public int Portee { get => portee; set => portee = value; }
    public int PV { get => pv; set => pv = value; }
    public int Force { get => force; set => force = value; }
    public bool Hostile { get => hostile; set => hostile = value; }
    #endregion

    #region Constructor
    public Animal(string name, bool hostile)
    {
        //this.Type = type;
        this.Name = name;
        this.hostile = hostile;
    }
    #endregion

    #region methods
    public virtual void Attaquer(Animal animal)
    {
        Console.WriteLine("J'attaque !");
    }
    #endregion
}