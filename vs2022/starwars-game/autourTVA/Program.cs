﻿Console.OutputEncoding = System.Text.Encoding.UTF8; // gérer le signe €

PreparerTVA();

void PreparerTVA()
{
    List<int> nomsTVA = new List<int>()
    {
        1,2,3
    };

    DemandeAjoutTVA(nomsTVA);

    var query = from tva in nomsTVA
                let monObjet = new { tva }
                orderby tva descending
                select monObjet;

    foreach (var tva in query)
    {
        grosseTVA(nomsTVA);
        Console.WriteLine(tva + " brute // ", tva + "€");
    }
}


void DemandeAjoutTVA(List<int> lesTVA)
{
    bool demandeArret = false;
    do
    {
        Console.WriteLine("Autre TVA ? ('zzz' pour arrêter)");
        var tvaSaisie = Console.ReadLine();

        if (!demandeArret && !lesTVA.Contains(Int32.Parse(tvaSaisie)))
        {
            lesTVA.Add(Int32.Parse(tvaSaisie));
        }
        else
        {
            demandeArret = true;
        }

    } while (!demandeArret);

    //Console.WriteLine("Donnes moi un montant HT");
    //int montantSaisie = Console.ReadLine();

    //montantTTC();
    //Console.WriteLine("montant TTC avec tes TVA :");
    //Console.WriteLine("");
}

void grosseTVA(List<int> nomsTVA)
{
    var query2 = from tva in nomsTVA
                 orderby tva descending
                 select tva;
    foreach (var tva in query2)
    {
        if (tva > 1.055)
        {
            Console.ForegroundColor = ConsoleColor.Green;
        }
        else
        {
            Console.ForegroundColor = ConsoleColor.White;
        }
    }

}

//void montantTTC()
//{

//}