﻿using zoo.api;

//avant héritage
//var animal = new Animal(AnimalType.Gorille, "Francois", true);
//Console.WriteLine($"{animal.Name}");

//avec heritage
var animal1 = new Gorille("Francois", true); // substitution => liskov
var animal2 = new Perroquet("Francine", false);
Console.WriteLine(animal1.Name + " " + animal2.Name); 

animal1.Attaquer(animal2);

Console.WriteLine(animal1); // override de tostring pour afficher un comportement par défaut de l'affichage


