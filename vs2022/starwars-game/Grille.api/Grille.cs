﻿using game.api;

namespace grille.api
{
	/// <summary>
	/// Grille du jeu
	/// </summary>
	public class Grille
	{
		#region Fields
		public int NbLigne { get; set; }
        public int NbColonne { get; set; }

		public string[,] tableauJeu;
        #endregion

        #region Constructor
        public Grille(int ligne, int colonne)
        {
            this.NbLigne = ligne;
			this.NbColonne = colonne;
			this.tableauJeu = new string[NbLigne,NbColonne];

			for(int i = 0; i < NbLigne; i++) 
			{
				for (int j = 0; j < NbColonne; j++)
				{
					tableauJeu[i, j] = "-";
				}
			}
        }
		#endregion

		#region Methods
		public string AfficherGrille()
		{
			string grilleJeu = "";
			for (int i = 0; i < NbLigne; i++)
			{
				for (int j = 0; j < NbColonne; j++)
				{
					grilleJeu += " | " + tableauJeu[i, j];
				}
				grilleJeu += " | \n";
			}
			return grilleJeu;		
		}

		public void Placement(Position2D positions, string perso)
		{
			tableauJeu[positions.X,positions.Y] = perso;
		}
		#endregion
	}
}