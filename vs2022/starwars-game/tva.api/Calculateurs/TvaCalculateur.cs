﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tva.api.Calculateurs
{
    public class TvaCalculateur
    {
        public decimal CalculerTTC(decimal montantHT, decimal tva)
        {
            return montantHT * (1 + tva);
        }
        public decimal CalculerTTC(decimal montantHT, FournirTvaCourante getTva)
        {
            return montantHT * (1 + getTva());
        }

        /// <summary>
        ///  Liste les tvas
        /// </summary>
        /// <param name="tvas"></param>
        /// <param name="filtre"></param>
        /// <returns></returns>
        public List<decimal> FiltrerTVA(List<decimal> tvas, decimal filtre = 0.055M)
        {

            // var query = from tva in tvas where tva >= filtre select tva;

            return tvas.Where(item => item >= filtre).ToList();
        }

        /// <summary>
        /// Demande la saisie de chaque tva à insérer dans la liste
        /// </summary>
        /// <param name="tvas">Liste de tvas, venant du program</param> 
        /// <param name="recupererSaisie"></param>
        /// <param name="afficher"></param>
        public void InitialiserListe(List<decimal> tvas, RecupererSaisie recupererSaisie, AfficherInformation afficher)
        {
            bool arretSaisie = false;

            do 
            {
                afficher("Nouvelle TVA ? ('STOP' pour arrêter)");
                var saisieUtilisateur = recupererSaisie();

                arretSaisie = saisieUtilisateur == "STOP";

                if(!arretSaisie)
                {
                    if(decimal.TryParse(saisieUtilisateur, out var tva))
                    {
                        tvas.Add(tva);
                    }
                }

            }while (!arretSaisie);
        }
    }
}
