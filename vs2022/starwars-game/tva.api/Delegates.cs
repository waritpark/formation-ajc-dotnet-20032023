﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tva.api
{
    /// <summary>
    /// Récupère la tva concernée pour le calcul du TTC
    /// </summary>
    /// <returns></returns>
    public delegate decimal FournirTvaCourante();
    /// <summary>
    /// Affichage de info vers une sortie utilisateur
    /// </summary>
    /// <param name="info"></param>
    public delegate void AfficherInformation(object info);

    /// <summary>
    /// Récupération de la saisie utilisateur
    /// </summary>
    /// <returns></returns>
    public delegate string RecupererSaisie();
}