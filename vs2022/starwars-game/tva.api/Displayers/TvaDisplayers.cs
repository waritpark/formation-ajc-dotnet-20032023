﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tva.api.Displayers
{
    /// <summary>
    /// Permet d'afficher une liste de TVA sous un format donné
    /// </summary>
    public class TvaDisplayers
    {
        #region Public methods
        /// <summary>
        /// Affiche la liste des TVAs
        /// </summary>
        /// <param name="tvaList"></param>
        public void Display(List<decimal> tvaList, AfficherInformation afficher) 
        {
            var query = from tva in tvaList
                        select new
                        {
                            TVABrut = tva,
                            TVAEuro = $"{tva} €"
                        };
            foreach (var item in query) 
            {
                afficher($"{item.TVABrut} // {item.TVAEuro}");
            }
        
        }
        #endregion
    }
}
