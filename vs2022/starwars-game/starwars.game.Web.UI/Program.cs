using Microsoft.EntityFrameworkCore;
using WebApplication1.Models;

var builder = WebApplication.CreateBuilder(args);

// injection de dependance ce fait avant le .build()
//va permettre d'auto instancier la classe DefaultDbContext
builder.Services.AddDbContext<DefaultDbContext>(options =>
{
	options.UseSqlServer(builder.Configuration.GetConnectionString("WebEntrainement"));
});

// Add services to the container.
builder.Services.AddControllersWithViews();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
}
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();
