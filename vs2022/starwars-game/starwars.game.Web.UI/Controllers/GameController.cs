﻿using Microsoft.AspNetCore.Mvc;
using starwars.game.Web.UI.Models;
using WebApplication1.Models;

namespace starwars.game.Web.UI.Controllers
{
    public class GameController : Controller
    {
		private readonly DefaultDbContext context;
		// Parceque j'ai ajouté la ligne AddDbContext dans le routeur
		// ici on va récupérer une instance deja prete
		public GameController(DefaultDbContext context)
		{
			this.context = context;
		}
		public IActionResult Index()
        {
            List<Game> list = new()
            {
                new() {  Id= 1, DateCreation = DateTime.Now, DateDebut = DateTime.Now },
                new() {  Id= 2, DateCreation = DateTime.Now, DateDebut = DateTime.Now }
            };
            List<CheckPoint> list2 = new()
            {
                new() { GameId = 1, PersoPointsDeVie = 90 },
                new() { GameId = 1, PersoPointsDeVie = 85 },
                new() { GameId = 2, PersoPointsDeVie = 75 },
                new() { GameId = 2, PersoPointsDeVie = 50 },
                new() { GameId = 2, PersoPointsDeVie = 20 }

            };
            return View("Index", new ListGame{ Liste1 = list, Liste2 = list2 });
        }

		[HttpGet]
		public IActionResult Add()
		{
			return View();
		}

		[HttpPost]
		public IActionResult Add(GameAddViewModel item)
		{
			this.context.Games.Add(item.Game);
			this.context.SaveChanges();

			return RedirectToAction("Index");
		}
	}
}
