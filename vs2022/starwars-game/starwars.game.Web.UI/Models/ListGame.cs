﻿namespace starwars.game.Web.UI.Models
{
    public class ListGame
    {
        public List<Game> Liste1 { get; set; }
        public List<CheckPoint> Liste2 { get; set; }
    }
}