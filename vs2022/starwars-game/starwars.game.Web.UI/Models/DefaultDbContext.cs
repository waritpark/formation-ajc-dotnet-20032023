﻿using Microsoft.EntityFrameworkCore;
using starwars.game.Web.UI.Models;
using System.Collections.Generic;

namespace WebApplication1.Models
{
	public class DefaultDbContext : DbContext
	{
		public DefaultDbContext(DbContextOptions options) : base(options)
		{
		}

		protected DefaultDbContext()
		{
		}

		/// <summary>
		/// C'est une Liste et elle sera connectée à une table en bdd
		/// </summary>
		public DbSet<Game> Games { get; set; }
	}
}
